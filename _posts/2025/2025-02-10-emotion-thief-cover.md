---
title: Emotion Thief Cover
lang: de-DE

tags:
  - Music

image: /blog/images/2025-02-14-emotion-thief.png
image_alt: Emotion Thief Cover, Art by Udelart

checker:
  - lt

long: true
seo_meta_fix: 0
excerpt: |
  Ich habe mich endlich aufgerafft, ein Cover auf Musikplattformen zu veröffentlichen!
  Passend zum Valetinstag gibt es meine deutsche Version von Emotion Thief, japanisches Orginal geschrieben von 40mP.
---
Ich habe mich endlich aufgerafft, ein Cover auf Musikplattformen zu veröffentlichen!
Passend zum Valetinstag gibt es meine deutsche Version von Emotion Thief, japanisches Orginal geschrieben von 40mP.

<!-- more -->

{:.icons.shields.justify-content-start}
[![Emotion Thief auf iTunes]({{ "/proxy/shields/-/itunes/shield.svg" | absolute_url }}){:loading="lazy" width="64" height="64"}](https://music.apple.com/de/album/emotion-thief-single/1795828797?uo=4&app=itunes&at=1001lry3&ct=dashboard)
[![Emotion Thief auf Spotify]({{ "/proxy/shields/-/spotify/shield.svg" | absolute_url }}){:loading="lazy" width="64" height="64"}](https://open.spotify.com/album/1GYdilhoCRU54PQW69dTiw?si=gCwJRTIiSDS92ncm0KGftA)
[![Emotion Thief auf Amazon Music]({{ "/proxy/shields/-/amazonmusic/shield.svg" | absolute_url }}){:loading="lazy" width="64" height="64"}](https://www.amazon.de/dp/B0DWKT54R4/)
[![Emotion Thief auf Apple Music]({{ "/proxy/shields/-/applemusic/shield.svg" | absolute_url }}){:loading="lazy" width="64" height="64"}](https://music.apple.com/de/album/emotion-thief-single/1795828797?uo=4&app=music&at=1001lry3&ct=dashboard)
[![Emotion Thief auf DEEZER]({{ "/proxy/shields/-/deezer/shield.svg" | absolute_url }}){:loading="lazy" width="64" height="64"}](https://www.deezer.com/album/710064391)
[![Emotion Thief auf iHeartRadio]({{ "/proxy/shields/-/iheartradio/shield.svg" | absolute_url }}){:loading="lazy" width="64" height="64"}](https://www.iheart.com/artist/id-45693240/albums/id-312794546)
[![Emotion Thief auf YouTube Music]({{ "/proxy/shields/-/youtubemusic/shield.svg" | absolute_url }}){:loading="lazy" width="64" height="64"}](https://www.youtube.com/watch?v=SSt6TKj-bag)
[![Emotion Thief auf TIDAL]({{ "/proxy/shields/-/tidal/shield.svg" | absolute_url }}){:loading="lazy" width="64" height="64"}](https://listen.tidal.com/album/416786837/)
[![Emotion Thief auf DistroKid]({{ "/proxy/shields/-/distrokid/shield.svg" | absolute_url }}){:loading="lazy" width="64" height="64"}](https://distrokid.com/hyperfollow/rpgreki/emotion-thief-2)

Vorschau:
<audio class="w-100" controls src="https://s3.amazonaws.com/audio.distrokid.com/preview_69014930_11B53B1E-B433-40DD-8560D75670C4903F.mp3"></audio>

<figure>
<picture>
  <source srcset="{{ '/blog/images/xs/2025-02-14-emotion-thief.avif' | absolute_url }}" media="(max-width: 575.96px)" type="image/avif">
  <source srcset="{{ '/blog/images/xs/2025-02-14-emotion-thief.webp' | absolute_url }}" media="(max-width: 575.96px)" type="image/webp">
  <source srcset="{{ '/blog/images/xs/2025-02-14-emotion-thief.png' | absolute_url }}" media="(max-width: 575.96px)" type="image/png">
  <source srcset="{{ '/blog/images/2025-02-14-emotion-thief.avif' | absolute_url }}" media="(min-width: 576px)" type="image/avif">
  <source srcset="{{ '/blog/images/2025-02-14-emotion-thief.webp' | absolute_url }}" media="(min-width: 576px)" type="image/webp">
  <source srcset="{{ '/blog/images/2025-02-14-emotion-thief.png' | absolute_url }}" media="(min-width: 576px)" type="image/png">
  <img loading="lazy" class="my-2" src="{{ '/blog/images/2025-02-14-emotion-thief.webp' | absolute_url }}" alt="Emotion Thief Cover" title="Emotion Thief Cover">
</picture>
  <figcaption class="text-center">Emotion Thief Cover, Art by <a href="https://vgen.co/Udelart">Udelart</a></figcaption>
</figure>

Lyrics by: Ken'ichi Rei

Im Licht der Suchscheinwerfer seh ich die Gefahr vor mir,\\
Gefühle, die ich spür’, erregen bald Verdacht\\
drum stehl’ ich mich davon, damit sie meine Spur ganz schnell verlier’n\\
und hab mich davon gemacht.

Die kalte, dunkle Nacht hüllt mich in ihre Schatten ein\\
auf meinem Weg zu dir, ich muss dich wiederseh’n.\\
Und muss ich mich dafür aus jeder noch so heiklen Kluft befrei’n\\
werd’ ich immer weitergeh’n.

Nur für dich werd’ ich jeden Code sofort entschlüsseln.\\
Brauchst du mich, werden alle Tür’n sich öffnen müssen.\\
Sollte ich, bevor mich der Alarm hier noch entlarvt,\\
mit dir dieser Dunkelheit entflieh’n?

Dieses stechende Gefühl in mir\\
bricht nun heraus, befreit mein bebendes Herz.\\
Ich hab Angst, dass ich dich dadurch verlier’,\\
Halt’ dich fest, verdräng’ den Schmerz.\\
Such den Ort, wo du in Sicherheit bist.\\
Und bis diese Nacht Vergangenheit ist,\\
Lass ich dich nie und nimmer, niemals wieder los

Obwohl das Zielfernrohr mich ganz präzise anvisiert,\\
und die Pistolenkugel in der Kammer liegt,\\
entkomme ich erneut, bis mich der Feind aus seiner Sicht verliert,\\
weil immer das Gute siegt.

Es ist mir gleich, ob ich dafür die Blicke auf mich zieh’,\\
mein Wille ist, dir zu gestehen, was ich fühl’.\\
Dein allererstes Lächeln, das du mir geschenkt hast, werd ich nie\\
verlieren, noch verspiel’n.

Selbst ein Schuss, der mir direkt mein Herz durchbohrte,\\
mein Entschluss, führt er mich auch zur Himmelspforte,\\
mir egal, solange ich die Kraft hab’, deine Hand\\
zu halten, und sei’s bis in den Tod.

Dieses magische Gefühl in mir,\\
ist so flüchtig wie ein nächtlicher Traum.\\
Drum sucht meine Hand im Dunkel nach dir\\
denn ich kann dir stets vertrau’n\\
Selbst wenn ich alles dafür aufgeben muss\\
und die Welt sich auch verschwört gegen mich.\\
Lass ich dich nie und nimmer, niemals wieder los.\\
Dieses stechende Gefühl in mir\\
bricht nun heraus, befreit mein bebendes Herz.\\
Und so schreie ich es laut heraus: “Gib mir\\
nur noch einmal deine Hand!”
